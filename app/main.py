"""API backend for web shop"""

# from os import getenv
from os import environ
from typing import Optional
from uuid import UUID

from dotenv import load_dotenv

from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse

import psycopg2
import psycopg2.extras

load_dotenv()

app = FastAPI()
psycopg2.extras.register_uuid()


def is_valid_uuid(uuid_to_test, version=4):
    """
    Checks is uuid input from API is valid format
    :param: uuid, uuid_version
    :return: bool
    """
    try:
        uuid_obj = UUID(uuid_to_test, version=version)
    except ValueError:
        return False

    return str(uuid_obj) == uuid_to_test


@app.on_event("startup")
def startup():
    """Open database connection on startup"""
    app.db = psycopg2.connect(  # pragma: no cover
        f"""postgresql://{
        environ['DB_USER']}:{environ['DB_PASSWORD']}@{environ['DB_HOST']}/{environ['DB']
        }"""
    )
    app.db.set_client_encoding('UNICODE')  # pragma: no cover


@app.on_event("shutdown")
def shutdown():
    """Close database connection on shutdown"""
    app.db.close()  # pragma: no cover


@app.get("/")
def root():
    """
    Endpoint for root
    :return: A small welcome message
    """
    return JSONResponse(content={"greeting": "Hello visitor!"})


@app.get("/stores")
def stores():
    """
    Endpoint to get name and full address of all stores in database
    :return: name and full address of all stores in database
    """
    # with app.db.cursor() as cur:
    app.db.cursor.execute(
        """
        SELECT stores.name, store_addresses.city, store_addresses.address, store_addresses.zip
        FROM stores
        FULL OUTER JOIN store_addresses
        ON stores.id=store_addresses.store
        """)
    db_response = app.db.cursor.fetchall()
    data = []
    for store in db_response:
        name, city, street, zip_code = store
        address = f"{street}, {zip_code} {city}"
        data.append({"name": name, "address": address})
    return JSONResponse(content={"data": data})


@app.get("/cities")
def cities(zipcode: Optional[int] = None):
    """
    Endpoint to get all unique cities or get city from zip code
    :param zipcode: Optional zip code to get city from
    :return: All unique cities where there is a store.
    If query parameter <zip> is given it filters by zip code
    """
    if zipcode:
        app.db.cursor.execute(
            """
            SELECT city
            FROM store_addresses
            WHERE zip='%s';
            """, (zipcode,))
        db_response = app.db.cursor.fetchall()
        if not db_response:
            raise HTTPException(status_code=404, detail="404 Not Found")
        # I suppose that city can't be null in db.
        # So db_response[0] will always contain name of a city
        return JSONResponse(content={"data": db_response[0]})
    app.db.cursor.execute("SELECT distinct city FROM store_addresses")
    city = app.db.cursor.fetchall()
    # I suppose that city can't be null in db.
    # So city_list[0] will always contain name of a city.
    return JSONResponse(content={"data": [city_list[0] for city_list in city]})


@app.get("/stores/{name}")
def stores_name(name: str):
    """
    Endpoint to get the full address of a store chosen by name
    :param name: Name to search for in database
    :return: Store specified by param: name
    """
    # with app.db.cursor() as cur:
    app.db.cursor.execute(
        """
        SELECT stores.name, store_addresses.city, store_addresses.address, store_addresses.zip
        FROM stores
        FULL OUTER JOIN store_addresses
        ON stores.id=store_addresses.store WHERE name = %s;
        """, (name.title(),))
    db_response = app.db.cursor.fetchall()
    if not db_response:
        raise HTTPException(status_code=404, detail="404 Not Found")
    data = []
    name, city, street, zip_code = db_response[0]
    address = f"{street}, {zip_code} {city}"
    data.append({"name": name, "address": address})
    return JSONResponse(content={"data": data})


@app.get("/sales")
def sales():
    """
    Endpoint to get sales
    :return: store, time, sales_id
    """
    app.db.cursor.execute(
        """
        SELECT stores.name, sales.time, sales.id
        FROM sales
        LEFT JOIN stores ON sales.store=stores.id
        """
    )
    db_response = app.db.cursor.fetchall()
    data = []
    for sale in db_response:
        name, date_time, sale_id = sale
        date_time = str(date_time).replace(" ", "T").replace("-", "")
        data.append({"store": name, "timestamp": date_time, "sale_id": str(sale_id)})
    return JSONResponse(content={"data": data})


@app.get("/sales/{sale_id}")
def sales_id(sale_id: str):
    """
    Endpoint to get sales on sales_id
    :param: sale_id
    :return:
    """
    if not is_valid_uuid(sale_id):
        raise HTTPException(status_code=422, detail="422 Unprocessable Entity")
    app.db.cursor.execute(
        """
            SELECT stores.name, sales.time, sales.id, products.name, sold_products.quantity
            FROM stores
            JOIN sales ON stores.id = sales.store
            JOIN sold_products ON sold_products.sale = sales.id
            JOIN products ON products.id = sold_products.product
            WHERE sales.id = %s;
            """, (sale_id,))
    db_response = app.db.cursor.fetchall()
    if not db_response:
        raise HTTPException(status_code=404, detail="404 Not Found")
    data = []
    for sale in db_response:
        name, date_time, db_id, prod_name, quantity = sale
        db_id = str(db_id)
        date_time = str(date_time).replace(" ", "T").replace("-", "")
        data.append(
            {
                "store": name,
                "timestamp": date_time,
                "sale_id": db_id,
                "products":
                    [
                        {
                            "name": prod_name,
                            "qty": quantity
                        }
                    ]
            }
        )
    return JSONResponse(content={"data": data})
