# u05-api-cicd

Group assignment u05-api-cicd  

An implementation of an API in Python with unit tests, documentation and ci/cd pipelines.  

## API  
This API is constructed with FastAPI and Psycopg2.  
To connect to our database you will need to add an environment file ```.env``` containing environment variables for the database connection.     

### Endpoints  
- GET /stores  
  - Returns name and full address of all stores in db  
- GET /stores/{name}  
  - Returns name and full address of one specific store by specifying its name  
- GET /cities  
  - Returns all unique cities where a store is located  
  - If query parameter ```zipcode``` is specified it returns only cities with that zipcode (ie. ```/cities?zipcode=12345```)  
- GET /sales  
  - Returns a list of all transactions  
- GET /sales/{sale-id}  
  - Returns information of a specific transaction by its UUID  


## Installation  
- First of all, create a virtual environment with the tool that fits your personality and masochistic tendencies.  
- Install required libraries with pip install -r requirements.txt  

## Usage  
Run Uvicorn from project root: ```uvicorn app.main:app```  
The API is now running on localhost where you can try it with Swagger UI.  

## Authors and acknowledgment  

Abdullah Wasuqe  
Carl Mikaelsson  
Daniel Goldman Lapington  
Erik Olsson  
Tomas Karlsson  
Viktor Berg 

## License  
This is free and unencumbered software released into the public domain.  

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.  

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.  

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.  

For more information, please refer to <https://unlicense.org>  
