"""Fixture for pytest"""
from unittest.mock import MagicMock

from fastapi.testclient import TestClient

import pytest


from u05.app.main import app


@pytest.fixture()
def fast_api_client():
    """fixture for tests. Sets client as TestClient and mocks db.connection"""
    client = TestClient(app)
    app.db = MagicMock()
    return client
